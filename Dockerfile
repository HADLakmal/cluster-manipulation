FROM asia.gcr.io/nextgen-devenv/golang-base-image:v1.0.1-dev-801ae99

WORKDIR /opt

COPY ./build /opt/build

EXPOSE 8888

ENTRYPOINT ["sh", "-c","./build"]
