module bitbucket.org/HADLakmal/cluster-manipulation
go 1.15

require (
	bitbucket.org/mybudget-dev/go-di v0.0.4
	cloud.google.com/go v0.84.0
	github.com/caarlos0/env/v6 v6.6.2
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.5.1
	github.com/tryfix/log v1.0.2
	github.com/tryfix/metrics v1.0.1
	golang.org/x/oauth2 v0.0.0-20210615190721-d04028783cf1 // indirect
	google.golang.org/api v0.48.0 // indirect
	google.golang.org/genproto v0.0.0-20210608205507-b6d2f5bf0d7d
)
