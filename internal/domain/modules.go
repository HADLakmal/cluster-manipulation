package domain

//go:generate mockgen -destination=../mocks/mock_type.go -package=mocks -source=./type.go

//
type Binding interface{}

//
type TypeInterface string

//
func (t TypeInterface) String() string {
	return string(t)
}

// Module bindings
const (
	ModuleLogger              TypeInterface = `modules.logger`
	ModuleMetricsReporter     TypeInterface = "modules.metrics-reporter"
	ModuleMetricsBaseReporter TypeInterface = "modules.base-metrics-reporter"
)
