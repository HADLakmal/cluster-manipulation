package conf

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/pkg/errors"
	"github.com/caarlos0/env/v6"
)

type GCPConf struct {
	JSONPath       string `env:"GOOGLE_APPLICATION_CREDENTIALS"`
	Name           string `env:"NAME"`
	Location       string `env:"LOCATION"`
	Description    string `env:"DESCRIPTION"`
	Network        string `env:"NETWORK"`
	SubNetwork     string `env:"SUBNETWORK"`
	Parent         string `env:"PARENT"`
	InitialCluster string `env:"INITIALCLUSTER"`
}

func (l *GCPConf) Register() error {
	err := env.Parse(l)
	if err != nil {
		return errors.Wrap(err, `error parsing logger config`)
	}

	return nil
}

