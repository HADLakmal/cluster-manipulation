package conf

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/pkg/errors"
	"github.com/caarlos0/env/v6"
)

type NodeDependencies struct {
	Name     string `env:"DEPENDENCIES_NAME"`
	Machine  string `env:"DEPENDENCIES_MACHINE"`
	DiskSize int32  `env:"DEPENDENCIES_DISK_SIZE"`
	DiskType string `env:"DEPENDENCIES_DISK_TYPE"`
	Version  string `env:"DEPENDENCIES_VERSION"`
}

func (l *NodeDependencies) Register() error {
	err := env.Parse(l)
	if err != nil {
		return errors.Wrap(err, `error parsing logger config`)
	}

	return nil
}
