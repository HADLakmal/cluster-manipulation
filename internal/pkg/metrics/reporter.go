package metrics

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/domain"
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/domain/adaptors"
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/pkg/errors"
	"bitbucket.org/mybudget-dev/go-di"
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/tryfix/metrics"
	"net/http"
)

type Reporter struct {
	reporter metrics.Reporter
	http     *http.Server
	logger   adaptors.Logger
	conf     *Config
}

func (m *Reporter) Run() error {
	m.logger.Info(fmt.Sprintf(`http server starting on %s/%s`, m.conf.HTTP.Port, m.conf.HTTP.Path))
	if err := m.http.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		return err
	}
	return nil
}

func (m *Reporter) Ready() chan bool {
	return nil
}

func (m *Reporter) Stop() error {
	c, fn := context.WithTimeout(context.Background(), m.conf.HTTP.ShutdownWait)
	defer fn()
	return m.http.Shutdown(c)
}

func (m *Reporter) Init(c di.Container) (err error) {
	conf := c.GetGlobalConfig(domain.ModuleMetricsReporter.String()).(*Config)

	m.Reporter(adaptors.ReporterConf{
		System:      conf.System,
		Subsystem:   conf.SubSystem,
		ConstLabels: map[string]string{},
	})

	m.logger = c.Resolve(domain.ModuleLogger.String()).(adaptors.Logger).NewLog(adaptors.LoggerPrefixed(`prometheus-server`))
	m.conf = conf
	m.http = &http.Server{
		Addr: conf.HTTP.Port,
	}

	router := mux.NewRouter()
	router.Handle(fmt.Sprintf(`/%s`, conf.HTTP.Path), promhttp.Handler())
	m.http.Handler = router

	c.Bind(domain.ModuleMetricsBaseReporter.String(), m.reporter)
	return nil
}

func (m *Reporter) Reporter(conf adaptors.ReporterConf) adaptors.MetricsReporter {
	m.reporter = metrics.PrometheusReporter(metrics.ReporterConf{
		System:      conf.System,
		Subsystem:   conf.Subsystem,
		ConstLabels: conf.ConstLabels,
	})
	return m
}

func (m *Reporter) Counter(conf adaptors.MetricConf) adaptors.Counter {
	return m.reporter.Counter(metrics.MetricConf{
		Path:        conf.Path,
		Labels:      conf.Labels,
		ConstLabels: conf.ConstLabels,
	})
}
func (m *Reporter) Observer(conf adaptors.MetricConf) adaptors.Observer {
	return m.reporter.Observer(metrics.MetricConf{
		Path:        conf.Path,
		Labels:      conf.Labels,
		ConstLabels: conf.ConstLabels,
	})
}
func (m *Reporter) Gauge(conf adaptors.MetricConf) adaptors.Gauge {
	return m.reporter.Gauge(metrics.MetricConf{
		Path:        conf.Path,
		Labels:      conf.Labels,
		ConstLabels: conf.ConstLabels,
	})
}
func (m *Reporter) GaugeFunc(conf adaptors.MetricConf, f func() float64) adaptors.GaugeFunc {
	return m.reporter.GaugeFunc(metrics.MetricConf{
		Path:        conf.Path,
		Labels:      conf.Labels,
		ConstLabels: conf.ConstLabels,
	}, f)
}
func (m *Reporter) Info() string {
	return m.reporter.Info()
}
func (m *Reporter) UnRegister(metrics string) {
	m.reporter.UnRegister(metrics)
}
