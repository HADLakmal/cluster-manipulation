package metrics

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/pkg/errors"
	"github.com/caarlos0/env/v6"
	"time"
)

type Config struct {
	System    string `env:"METRICS_SYSTEM" envDefault:"pbs"`
	SubSystem string `env:"METRICS_SUBSYSTEM" envDefault:"golang_restful_streaming"`
	HTTP      struct {
		Path         string        `env:"METRICS_HTTP_PATH" envDefault:"metrics"`
		Port         string        `env:"METRICS_HTTP_HOST" envDefault:":7001"`
		ShutdownWait time.Duration `env:"METRICS_HTTP_SERVER_SHUTDOWN_WAIT" envDefault:"5s"`
	}
}

func (r *Config) Register() error {
	err := env.Parse(r)
	if err != nil {
		return errors.Wrap(err, `error loading metrics Config`)
	}
	return nil
}
