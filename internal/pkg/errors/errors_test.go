package errors_test

import (
	e "errors"
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/pkg/errors"
	"regexp"
	"testing"
)

func TestNewError(t *testing.T) {
	e := errors.New("sample error message")
	if e == nil {
		t.Error("expected non empty error received nil")
		return
	}

	errString := e.Error()
	match, err := regexp.Match("sample error message at.*", []byte(errString))
	if err != nil {
		t.Error(err)
		t.Fail()
		return
	}

	if !match {
		t.Fail()
		return
	}
}

func TestNewEmbeddedError(t *testing.T) {
	errOne := errors.New("error one")
	errTwo := errors.Wrap(errOne, "error two")

	e := e.Unwrap(errTwo)
	if e.Error() != errOne.Error() {
		t.Errorf("\nneed: %v \n got: %v", errOne, e)
	}
}
