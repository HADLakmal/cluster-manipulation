package errors

import (
	"regexp"
	"testing"
)

func TestFilePath(t *testing.T) {
	path := filePath()

	pattern := "^at testing.tRunner.*"
	match, err := regexp.Match(pattern, []byte(path))
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	if !match {
		t.Errorf("\nshould match pattern: %s \ngot: %s", pattern, path)
	}
}
