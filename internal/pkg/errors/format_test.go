package errors_test

//import (
//	"fmt"
//	"net/http"
//	errors2 "search-proxy/internal/http/errors"
//	"bitbucket.org/HADLakmal/cluster-manipulation/internal/pkg/errors"
//	"testing"
//)
//
//type ProducerError struct {
//	error
//}
//
//type StoreError struct {
//	error
//}
//
//type ValidatorError struct {
//	error
//}
//
//func newValidatorError(msg string, cause error) error {
//	return ValidatorError{
//		errors.Wrap(cause, msg),
//	}
//}
//
//// TestFormat tests formatting.
//// go test -v -run ^TestFormat$ golang-restfull-streaming/internal/pkg/errors
//func TestFormat(t *testing.T) {
//	errOne := errors.New("error 1")
//	errTwo := ProducerError{errors.Wrap(errOne, "error 2")}
//	errThree := StoreError{errors.Wrap(errTwo, "error 3")}
//	errFour := ValidatorError{errors.Wrap(errThree, "error 4")}
//	errFive := newValidatorError("error 5", errFour)
//
//	fmt.Println(errFive.Error())
//
//	fmt.Printf("%+v\n-------\n", format(errOne))
//	fmt.Printf("%+v\n-------\n", format(errTwo))
//	fmt.Printf("%+v\n-------\n", format(errThree))
//	fmt.Printf("%+v\n-------\n", format(errFour))
//	fmt.Printf("%+v\n-------\n", format(errFive))
//}

//func format(err error) errors2.ErrorResponse {
	//switch err.(type) {
	//case ProducerError:
	//	return errors2.ErrorResponse{
	//		Description: "Producer error",
	//		StatusCode:  http.StatusBadRequest,
	//	}
	//case StoreError:
	//	return errors2.ErrorResponse{
	//		Description: "Store error",
	//		StatusCode:  http.StatusBadRequest,
	//	}
	//case ValidatorError:
	//	return errors2.ErrorResponse{
	//		Description: "Validator error",
	//		StatusCode:  http.StatusUnprocessableEntity,
	//	}
	//default:
	//	return errors2.ErrorResponse{
	//		Description: "Unknown error",
	//		StatusCode:  http.StatusInternalServerError,
	//	}
	//}
//}
