package node_pool

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/conf"
	container "cloud.google.com/go/container/apiv1"
	"context"
	containerpb "google.golang.org/genproto/googleapis/container/v1"
)

func ScaleChange(ctx context.Context, name string, count int32, cnf conf.GCPConf, client *container.ClusterManagerClient) (*containerpb.Operation, error) {
	return client.SetNodePoolSize(ctx, &containerpb.SetNodePoolSizeRequest{
		NodeCount: count,
		Name:      name,
		Zone:      cnf.Location,
		ProjectId: cnf.Parent,
		ClusterId: cnf.Name,
		NodePoolId: name,
	})
}
