package internal

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/cluster"
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/conf"
	node_pool "bitbucket.org/HADLakmal/cluster-manipulation/internal/node-pool"
	container "cloud.google.com/go/container/apiv1"
	"context"
	containerpb "google.golang.org/genproto/googleapis/container/v1"
	"log"
)

func Boot() {
	cnf := conf.GCPConf{}
	if err := cnf.Register(); err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	nodeDep := conf.NodeDependencies{}
	if err := nodeDep.Register(); err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	ctx := context.Background()
	c, err := container.NewClusterManagerClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	depNode := conf.NodeDependencies{}
	if err = depNode.Register(); err != nil {
		log.Fatal(err)
	}

	nodes := []*containerpb.NodePool{
		{
			Name: depNode.Name,
			Config: &containerpb.NodeConfig{
				MachineType: depNode.Machine,
				DiskSizeGb:  depNode.DiskSize,
				Preemptible: false,
				DiskType:    depNode.DiskType,
			},
			InitialNodeCount: 1,
			Version:          depNode.Version,
		},
	}

	cluster.Create(ctx, cnf, nodes, c)

	_, err = node_pool.ScaleChange(ctx, depNode.Name, 0, cnf, c)
	if err != nil {
		log.Fatal(err)
	}

	cluster.Delete(ctx, cnf, c)

	log.Print("cluster creation done")
}
