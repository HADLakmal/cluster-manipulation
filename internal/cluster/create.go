package cluster

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/conf"
	container "cloud.google.com/go/container/apiv1"
	"context"
	containerpb "google.golang.org/genproto/googleapis/container/v1"
	"log"
	"time"
)

func Create(ctx context.Context, cnf conf.GCPConf, nodes []*containerpb.NodePool, client *container.ClusterManagerClient) *containerpb.Operation {


	req := &containerpb.CreateClusterRequest{
		Cluster: &containerpb.Cluster{
			Name:        cnf.Name,
			Description: cnf.Description,
			Network:     cnf.Network,
			Subnetwork:  cnf.SubNetwork,
			Locations:   []string{cnf.Location},
			IpAllocationPolicy: &containerpb.IPAllocationPolicy{
				UseIpAliases: true,
			},
			DefaultMaxPodsConstraint: &containerpb.MaxPodsConstraint{
				MaxPodsPerNode: 100,
			},
			ShieldedNodes: &containerpb.ShieldedNodes{
				Enabled: true,
			},
			ReleaseChannel: &containerpb.ReleaseChannel{
				Channel: containerpb.ReleaseChannel_REGULAR,
			},
			DatabaseEncryption: &containerpb.DatabaseEncryption{
				State: containerpb.DatabaseEncryption_DECRYPTED,
			},
			InitialClusterVersion: cnf.InitialCluster,
			Location:              cnf.Location,
			NodePools:             nodes,
		},
		Parent:    cnf.Parent,
		ProjectId: cnf.Parent,
		Zone:      cnf.Location,
	}
	op, err := client.CreateCluster(ctx, req)
	if err != nil {
		log.Panic(err)
	}

	clusterCreated := make(chan bool)

	go func(ch chan bool) {
		getClusterError := func() error {
			for {
				time.Sleep(5 * time.Second)
				getCluster, getClusterError := client.GetCluster(ctx,
					&containerpb.GetClusterRequest{Name: cnf.Name, Zone: cnf.Location, ProjectId: cnf.Parent, ClusterId: cnf.Name})
				log.Printf(`err : %v , cluster status : %s`, err, getCluster.GetStatus())
				if err != nil {
					return getClusterError
				}

				if containerpb.Cluster_RUNNING <= getCluster.GetStatus() {
					ch <- true
				}
			}
		}()
		if getClusterError != nil {
			log.Fatal(getClusterError)
		}
	}(clusterCreated)

	<-clusterCreated

	return op
}
