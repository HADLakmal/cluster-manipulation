package cluster

import (
	"bitbucket.org/HADLakmal/cluster-manipulation/internal/conf"
	container "cloud.google.com/go/container/apiv1"
	"context"
	containerpb "google.golang.org/genproto/googleapis/container/v1"
	"log"
	"time"
)

func Delete(ctx context.Context, cnf conf.GCPConf, client *container.ClusterManagerClient) *containerpb.Operation {
	op, err := client.DeleteCluster(ctx,
		&containerpb.DeleteClusterRequest{Name: cnf.Name, Zone: cnf.Location, ProjectId: cnf.Parent, ClusterId: cnf.Name})
	if err != nil {
		log.Fatal(err)
	}

	clusterDeleted := make(chan bool)

	go func(ch chan bool) {
		getClusterError := func() error {
			for {
				time.Sleep(5 * time.Second)
				getCluster, getClusterError := client.GetCluster(ctx,
					&containerpb.GetClusterRequest{Name: cnf.Name, Zone: cnf.Location, ProjectId: cnf.Parent, ClusterId: cnf.Name})
				log.Printf(`err : %v , cluster status : %s`, err, getCluster.GetStatus())
				if err != nil {
					return getClusterError
				}

				if containerpb.Cluster_STOPPING <= getCluster.GetStatus() {
					ch <- true
				}
			}
		}()
		if getClusterError != nil {
			log.Fatal(getClusterError)
		}
	}(clusterDeleted)

	<-clusterDeleted

	return op
}
